<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 17.11.2015
 * Time: 20:50
 */

require ($_SERVER['DOCUMENT_ROOT'].'/engine/phpmailer/PHPMailerAutoload.php');
include($_SERVER['DOCUMENT_ROOT'].'/engine/classes/DB.php');
//$db = new DB();

$input = $_REQUEST;
$response = array();

if (isset($input['action'])){
    $response['action'] = $input['action'];
    if (isset($input['message'])){ $response['message'] = $input['message']; }

    switch ($input['action']){
        case "callback":
           /* $response['replication'] = $db->add(
                array('source','client_phone','date_created','content'),
                array($input['params']['source'],$input['params']['client_phone'],date("Y-m-d H:i:s"),$input['message']),
                "requests"); */
            $response['response'] = sendMail($input);
            break;
        case "calc":

          /*  $response['replication'] = $db->add(
                array(
                    'source',
                    'client_phone',
                    'client_name',
                    'client_email',
                    'city',

                    'car_name',
                    'car_power',
                    'car_age',
                    'car_isCargo',
                    'driver_age',
                    'driver_experience',
                    'nolimit_insurance',
                    'total',
                    'discount',

                    'date_created',
                    'content'),
                array(
                    $input['params']['source'],
                    $input['params']['client_phone'],
                    $input['params']['client_name'],
                    $input['params']['client_email'],
                    $input['params']['city'],

                    $input['params']['car_name'],
                    $input['params']['car_power'],
                    $input['params']['car_age'],
                    $input['params']['car_isCargo'],
                    $input['params']['driver_age'],
                    $input['params']['driver_experience'],
                    $input['params']['nolimit_insurance'],
                    $input['params']['total'],
                    $input['params']['discount'],

                    date("Y-m-d H:i:s"),
                    $input['message']),

                "requests_calc"); */

            $input['heading'] = "Заявка с калькулятора";
            $response['response'] = sendMail($input);
            break;
        case "cmd":
            DB::cmd($input);
            break;
        case "documents":
            $input['heading'] = "Документы для расчета страховки";


            $response['response'] = sendMail($input);
            break;
        case "agent":
            $input['heading'] = "Заявка на партнерство";
           /* $response['replication'] = $db->add(
                array('source','client_phone','client_name','date_created','content'),
                array(
                    $input['params']['source'],
                    $input['params']['client_phone'],
                    $input['params']['client_name'],
                    date("Y-m-d H:i:s"),
                    $input['message']),
                "requests"); */
            $response['response'] = sendMail($input);
            break;
        default:
            break;
    }
    print json_encode($response);
}

// set up mailer

function sendMail($data){
    $mail = new PHPMailer;
    $mail->CharSet="utf-8";

    //$from = "lead_".date("d-m-Y__H-i");
   //$from = "robot";
    $mail->setFrom("robot@osago-msk.com");
    $mail->addAddress('osagomsk7@gmail.com');
    $mail->addAddress('e-osago-msk@yandex.ru');

    if (isset($data['heading'])){$mail->Subject = $data['heading'];} else {
        $mail->Subject = 'Новая заявка!';
    }

    if (isset($data['message'])){ $mail->msgHTML($data['message']); }{
        if(!$mail->send()) {
            return 'Message could not be sent.'.$mail->ErrorInfo;
        } else {
            return "OK";
        }
    }
}


