/**
 * Created by Alex on 28.03.2016.
 */


var files;
$(function(){
    $('.sender').click(function(){
        var validated = true;
        var thus = this;
        var parent = $( $(this).attr('data-parent') );
        var fields = $(parent).find('.input');
        var action = "callback";
        var message = "<h3>" + $(this).attr('data-heading') + "</h3>";
        var metrica_callback = $(this).attr('data-metrika-id');
        if ($(this).attr('data-action') != ""){ action = $(this).attr('data-action'); }

        for (var i=0; i < fields.length; i++){

            if ($(fields[i]).hasClass('required') && $(fields[i]).val() == ""){
                console.log( $(fields[i]).parent());
                $(fields[i]).parent().addClass('has-error');
                validated = false;
            }
        }

        params = {
            source : $(this).attr('data-heading')
        };
        var j = 0;
        if (validated){
            for (var i=0; i < fields.length; i++){
                message += $(fields[i]).attr('data-label') + ": <strong>"+$(fields[i]).val()+"</strong><br>";

                if ($(fields[i]).attr('data-property') != undefined){
                    var param = {};
                    var name = ($(fields[i]).attr('data-property'));
                    var val = $(fields[i]).val();
                    param[name]=val ;

                    params[name] = val;
                    j++;
                }

            }


            var data = {
                action: action,
                message: message,
                params: params
            };

            console.log(data);

            $.ajax({
                type: 'POST',
                url: '/engine/ajax.php',
                data: data,
                success: function (data) {
                    var response = JSON.parse(data);

                    if (response.response == "OK") {

                        $(parent).find('.responder').html("Сообщение успешно отправлено!")
                            .addClass('alert-success')
                            .removeClass('alert-danger')
                            .removeClass('hidden');
                     //   $(thus).addClass('hidden');
                        if (metrica_callback && metrica_callback != "") {
                            console.log('Metrika reachGoal ' + metrica_callback);
                            //yaCounter45954075.reachGoal(metrica_callback)
                        }
                    } else {
                        $(parent).find('.responder').html("Произошла ошибка! Сообщение не отправлено!")
                            .addClass('alert-danger')
                            .removeClass('hidden');
                    }
                },
                timeout: function () {
                    _response = 0;
                },
                error: function () {
                    _response = 0;
                    $(parent).find('.responder').html("Ошибка связи с сервером! Проверьте ваш интернет или повторите позднее.");
                }
            });


        } else {

            $(parent).find('.responder').html("Ошибка! Вы не заполнили все необходимые поля!")
                .addClass('alert-danger')
                .removeClass('alert-success')
                .removeClass('hidden');
        }
    });


    // действия перед показлом окна
    $('#callbackModal').on('shown.bs.modal', function (e) {

        var target = e.relatedTarget;
        var _title = "Обратный звонок";
        var action = $(target).attr('data-action');

        if ( $(target).attr('data-title')){ _title = ( $(target).attr('data-title') ); }
        if ( $(target).attr('data-action') == "diagnostic_card"){
            $('#callbackModal .sender')
                .attr('data-metrika-id', $(target).attr('data-action'))
                .attr('data-heading', $(target).attr('data-title'));
        } else {
            $('#callbackModal .sender')
                .attr('data-metrika-id', 'callback_submit')
                .attr('data-heading', 'Заявка на обратный звонок');
        }

        $('#callbackModal .modal-title').html(_title);
        $('#callbackModal .sender').attr('data-heading', _title );

        var form_h = $('#callbackModal .modal-dialog').height();
        var doc_h = $(window).height();

        if (doc_h - form_h > 200){
            var mTop = (doc_h - form_h) / 3;
            $('#callbackModal .modal-dialog').css('margin-top', mTop);
        }
    });

    // действия после закрытия окна
    $('#callbackModal').on('hidden.bs.modal', function (e) {
        var parent = $('#callbackModal');

        $(parent).find('.form-control').val("");
        $(parent).find('.alert').addClass('hidden');

    })
    
});