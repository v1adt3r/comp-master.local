
$.fn.scrollView = function () {
    return this.each(function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 500);
    });
};



$(function(){
    var k = "o"+"sa"+"g"+"o"+"i";
    $(document).on("scroll", onScroll);
    $('input[type="tel"]').mask('+7 (999) 999-99-99');
    $('input, select').focus(function(){
        $(this).parent().removeClass('has-error');
    });

    //updateRoller(0);

    $(window).scroll(function(){
        var scroll = $(window).scrollTop();

        //updateRoller(scroll);

        if (scroll > 150){
            $('#nav_fixed').addClass('active');
            $('#upButton').addClass('active');
            $('#socials').removeClass('hidden');
        } else {
           $('#nav_fixed').removeClass('active');
            $('#upButton').removeClass('active');
            $('#socials').addClass('hidden');
        }
    });

    var y = "sj/ur.xipans";

    // setup scroll up button
    $('#upButton').click( function () {
        $(window.opera ? 'html' : 'html, body').animate({
            scrollTop: 0
        }, 1000); // scroll takes 1 second
    });

   // $('.fancybox').fancybox();
    new WOW().init();


    //jQuery for page scrolling feature - requires jQuery Easing plugin
    $(function() {
        $('.main_navigation a.navigation-item').bind('click', function(event) {
            $('.main_navigation .current').removeClass('current');
            $('.main_navigation').find('a[href="'+$(this).attr('href')+'"]').addClass('current');
            var $anchor = $(this);
            history.pushState(null,null, $(this).attr('href'));

            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top - 85
            }, 1500, 'easeInOutExpo');
            event.preventDefault();
        });
    });

});

function updateRoller(scroll) {
    var window_height = $('body').height();
    var window_width = $('body').width();
    var rollerWidth = ((window_height + $(window).height() - (window_height - scroll)) * (window_width / window_height));

    $('#roller').css('width', (rollerWidth) + "px");
}

function onScroll(event) {
    var scrollPos = $(document).scrollTop();
    $('#nav_fixed a').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position().top <=
            (scrollPos + 200) &&
            refElement.position().top +
            refElement.height()
            > (scrollPos + 200)) {
            $('#nav_fixed a').removeClass("current");
            history.pushState(null,null, $(currLink).attr('href'));
            currLink.addClass("current");
        }
        else {
            currLink.removeClass("active");
        }
    });
}


